# Project Documentation

## Introduction
This project is a basic website developed using Bootstrap. It showcases a casino-themed webpage with various game options and a cookie consent banner.

## File Structure
The project consists of the following key files:
- `index.html`: Main HTML file containing the website layout.
- `assets/`: Directory holding images and other assets used in the website.
- `js/scripts.js`: JavaScript file containing custom functionalities for the casino games.
- `datenschutz.html`: Placeholder page for privacy policy information.
- Other supporting Bootstrap and JavaScript library files.

## Features Implemented
- Navigation bar with links to different sections of the website (Home, About, Products, Store).
- Sections for displaying casino-related content, including game descriptions and options.
- Interactive buttons for various casino games like Lottery Quick Pick, Keno Quick Pick, Coin Flipper, Dice Roller, Playing Card Shuffler, and Birdie Fund Generator.
- Footer section with copyright information and a placeholder for the Impressum (Imprint) content.
- Cookie consent banner at the bottom of the page for cookie management.

## Instructions
1. To view the website, open `index.html` in a web browser.
2. Navigate through the site using the links in the navigation bar to explore different sections.
3. Click on the various game buttons (Lottery Quick Pick, Keno Quick Pick, etc.) to interact with the casino games.
4. To accept cookies, click the "Accept cookies" button on the cookie consent banner.
5. To reject cookies, click the "Reject cookies" button on the cookie consent banner.

## Code Comments Overview
- The HTML structure includes Bootstrap components for responsiveness and styling.
- JavaScript functions handle the acceptance and rejection of cookies based on user interaction.
- The `scripts.js` file contains functions utilizing the Random.org API to generate random numbers and simulate casino game results.
- Additional comments are provided within the code for better understanding of functionalities.

## Conclusion
The project demonstrates the use of Bootstrap for creating a casino-themed webpage with various interactive game options and compliance with cookie consent requirements.


